using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PalqueAléatoire : MonoBehaviour
{
    public GameObject plaque;
    // Start is called before the first frame update
    void Start()
    {
        int x = Random.Range(0, 101) * 3;
        int z = Random.Range(0, 101) * 3;
        plaque.transform.position = new Vector3(x,0,z);
    }
}
