using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScriptQuit : MonoBehaviour
{

    public Button quit;
    public GameObject fondMenu;
    public bool escapeMenuOpen;
    public GameObject scriptadesactiver;
    public GameObject scriptaarreter;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (escapeMenuOpen == false)
            {
                fondMenu.gameObject.SetActive(true);
                scriptadesactiver.GetComponent<MouselookSrc>().enabled = false;
                scriptaarreter.GetComponent<PlayerMouvement>().enabled = false;
                escapeMenuOpen = true;
                quit.gameObject.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                fondMenu.gameObject.SetActive(false);
                scriptadesactiver.GetComponent<MouselookSrc>().enabled = true;
                scriptaarreter.GetComponent<PlayerMouvement>().enabled = true;
                escapeMenuOpen = false;
                quit.gameObject.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
    public void BackMenu()
    {
        SceneManager.LoadScene("Menu");
    }

}
